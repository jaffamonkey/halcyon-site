---
path: '/clients'
date: '2019-05-04'
title: 'Who I have helped'
---

#### Mobile, Broadcasting, Gaming, Publishing, Finance, Creative, eCommerce, Public Sector, Property, Utilities

* Avison Young
* BBC
* BOC
* BSi
* Citibank
* Digital Jigsaw
* Digitas
* Disney Internet Group
* Elvis Communications
* Emap
* The Engine Group
* FortuneCookie
* Gouden Gids
* GVA Grimley
* Amerada Hess
* Hiveworks
* i2N
* Incisive Media
* KPMG
* Misys
* Multex Investor
* Nike
* O2
* Pearson (Education)
* Perot
* Providem Media
* Reuters
* Rufusleonard
* Sema
* Seven
* Playstation
* Spotless interactive
* Tele2
* Tixdaq
* TNT
* Unisys
* Westfield
* Wowcher
* Nokia
* Vodafone
* Belgacom
* Sony