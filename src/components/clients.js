import React from 'react';
import { media } from '@styles';
import styled from 'styled-components';
import BBC from '../images/bbc.png';
import Disney from '../images/disney.png';
// import Engine from '../images/engine.png';
import o2 from '../images/o2.png';
import Playstation from '../images/ps.png';
// import Digitas from '../images/digitas.png';
// import Emap from '../images/emap.png';
import Tele2 from '../images/tele2.png';
// import IncisiveMedia from '../images/incisive.png';
// import Hiveworks from '../images/hiveworks.png';

const Links = styled.div`
  text-align: center;
  img {
    padding: 10px;
    ${media.tablet`height: 40px;`};
  }
`;

const Clients = () => (
  <Links>
    <img src={Disney} alt="Disney" />
    <img src={BBC} alt="bbc" />
    <img src={o2} alt="o2" />
    <img src={Playstation} alt="Playstation" />
    <img src={Tele2} alt="Tele2" />
  </Links>
);

export default Clients;
