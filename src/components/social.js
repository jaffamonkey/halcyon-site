import React from 'react';
import { media } from '@styles';
import styled from 'styled-components';
import Twitter from '../images/twitter.png';
import LinkedIn from '../images/linkedin.png';
import Email from '../images/email.png';
import Mobile from '../images/mobile.png';

const Links = styled.div`
  text-align: center;
  img {
    height: 60px;
    width: 60px;
    padding: 10px;
    ${media.tablet`height: 60px; width: 60px;`};
  }
`;

const Social = () => (
  <Links>
    <a href="https://twitter.com/jaffamonkey">
      <img src={Twitter} alt="twitter" />
    </a>{' '}
    <a href="https://www.linkedin.com/in/jaffamonkey/">
      <img src={LinkedIn} alt="linkedin" />
    </a>
    <a href="mailto:paullittlebury@gmail.com">
      <img src={Email} alt="email" />
    </a>
    <a href="sms:+447739670315">
      <img src={Mobile} alt="mobile" />
    </a>
<p>&copy;2019 jaffamonkey B.V. (KVK 71855157) Vestigingsnr: 1012 RP Amsterdam</p>
  </Links>
);

export default Social;
