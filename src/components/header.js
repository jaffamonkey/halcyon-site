import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { theme, media } from '@styles';
const { colors } = theme;
import Logo from '../images/logo.png';

const HeaderContainer = styled.header`
  padding: 20px 25px;
  text-align: center;
  ${media.tablet`padding: 70px 10px;`};
  img {
    width: 100px;
    padding-right: 20px;
    vertical-align: middle;
    padding-bottom: 20px;
  }
`;
const Title = styled.h1`
  color: ${colors.accent};
  font-size: 70px;
  font-weight: 500;
  background: -webkit-linear-gradient(${colors.yellow}, ${colors.accent});
  background: linear-gradient(${colors.yellow}, ${colors.accent});
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
  ${media.tablet`font-size: 40px;`};
`;
const Subtitle = styled.h2`
  color: ${colors.lightGrey};
  font-size: 22px;
  line-height: 2;
  font-weight: 400;
  ${media.tablet`
    font-size: 18px;
  `};
`;
const TitieLink = styled.a`
  text-decoration: none;
`;

const Header = ({ title }) => (
  <HeaderContainer>  
    <img src={Logo} alt="logo" />
    <TitieLink href="/">
      <Title>{title}</Title>
    </TitieLink>
  </HeaderContainer>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default Header;
