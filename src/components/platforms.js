import React from 'react';
import styled from 'styled-components';
import { theme, media } from '@styles';
const { colors, fontSizes } = theme;
import Opensource from '../images/logos/ai.png';
import Training from '../images/logos/training.png';
import QualityAssurance from '../images/logos/build.png';
import TestEngineering from '../images/logos/ci.png';

const PlatformsContainer = styled.section`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 20px;
  justify-content: center;
  padding: 30px;
  width: 100%;
  text-align: center;
  max-width: 980px;
  margin-top: 10px;
  margin-bottom: 10px;
  margin: auto;
  ${media.tablet`
    padding: 10px;
    margin: auto;
  `};
  ${media.phoneXL`
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-gap: 10px;
    padding: 10px;
    margin: auto;
  `};
`;
const Platform = styled.a`
  align-items: center;
  justify-content: space-around;
  background-color: ${colors.active};
  padding: 30px 5px;
  text-align: center;
  font-size: 16px;
  border-radius: 4px;
  transition: ${theme.transition};
  ${media.phoneXL`padding: 20px;`};

  &:hover,
  &:focus {
    background-color: ${colors.hover};
    transform: translateY(-5px);
    outline: 0;
  }

  img {
    height: 80px;
    width: auto;
    ${media.tablet`height: 50px; width: auto;`};
  }
`;
const PlatformName = styled.div`
  margin-top: 20px;
  color: ${colors.white};
  font-size: ${fontSizes.base};
  line-height: 1.5;
`;

const platforms = [
  {
    name: 'About',
    url: '/about',
    logo: Opensource,
  },
  {
    name: 'Quality assurance',
    url: '/qualityassurance',
    logo: QualityAssurance,
  },
  {
    name: 'Test engineering',
    url: '/testengineering',
    logo: TestEngineering,
  },
  {
    name: 'Training',
    url: '/training',
    logo: Training,
  },
  // {
  //   name: 'Clients',
  //   url: '/clients',
  //   logo: Clients,
  // },
];

const Platforms = () => (
  <PlatformsContainer>
    {platforms.map(({ name, url, logo }, i) => (
      <Platform key={i} href={url} rel="noopener noreferrer">
        <img src={logo} alt={name} />
        <PlatformName>{name}</PlatformName>
      </Platform>
    ))}
  </PlatformsContainer>
);

Platforms.propTypes = {};

export default Platforms;
