import Colors from './colors';
import Footer from './footer';
import Head from './head';
import Header from './header';
import Layout from './layout';
import Platforms from './platforms';
import Social from './social';
import Clients from './clients';

export { Colors, Footer, Head, Header, Layout, Platforms, Social, Clients };
