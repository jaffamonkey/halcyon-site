import React from 'react';
import styled from 'styled-components';
import { Layout } from '@components';
import { theme, media } from '@styles';
const { colors } = theme;
import { graphql } from 'gatsby';
import Opensource from '../images/logos/ai.png';
import Training from '../images/logos/training.png';
import QualityAssurance from '../images/logos/build.png';
import TestEngineering from '../images/logos/ci.png';
import Clients from '../images/logos/deploy.png';

const SubMenu = styled.section`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  grid-gap: 10px;
  justify-content: center;
  width: 100%;
  max-width: 600px;
  margin: 0px auto 40px;
  ${media.tablet`
    padding: 0px;
    margin: 0px auto 40px;
  `};
  ${media.phoneXL`
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-gap: 10px;
  `};
`;

const Platform = styled.a`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${colors.active};
  padding: 15px;
  text-align: center;
  font-size: 16px;
  border-radius: 4px;
  transition: ${theme.transition};
  ${media.phoneXL`padding: 10px;`};

  &:hover,
  &:focus {
    background-color: ${colors.hover};
    transform: translateY(-5px);
    outline: 0;
  }

  img {
    width: 50px;
    ${media.tablet`width: 25px;`};
  }
`;

const Title = styled.h1`
  color: ${colors.accent};
  font-size: 40px;
  font-weight: 500;
  text-align: center;
  background: -webkit-linear-gradient(${colors.blue}, ${colors.accent});
  background: linear-gradient(${colors.blue}, ${colors.accent});
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: #3399ff;
  ${media.tablet`font-size: 25px;`};
`;

const MdText = styled.div`
  color: ${colors.lightGrey};
  font-size: 16px;
  line-height: 2;
  font-weight: 400;
  width: 100%;
  max-width: 980px;
  margin: 20px auto 20px;
  ${media.tablet`
  margin: 20px auto 20px;
  padding: 20px;
    font-size: 14px;
  `};
`;

const platforms = [
  {
    name: 'About',
    url: '/about',
    logo: Opensource,
  },
  {
    name: 'Quality assurance',
    url: '/qualityassurance',
    logo: QualityAssurance,
  },
  {
    name: 'Test engineering',
    url: '/testengineering',
    logo: TestEngineering,
  },
  {
    name: 'Training',
    url: '/training',
    logo: Training,
  }
];

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data; // data.markdownRemark holds our post data
  const { frontmatter, html } = markdownRemark;
  return (
    <Layout>
      <SubMenu>
        {platforms.map(({ name, url, logo }, i) => (
          <Platform key={i} href={url} rel="noopener noreferrer">
            <img src={logo} alt={name} />
          </Platform>
        ))}
      </SubMenu>
      <Title>{frontmatter.title}</Title>
      {/* <Content dangerouslySetInnerHTML={{ __html: html }} /> */}
      <MdText dangerouslySetInnerHTML={{ __html: html }} />
    </Layout>
  );
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
      }
    }
  }
`;
