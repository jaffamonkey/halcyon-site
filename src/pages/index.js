import React from 'react';
import { Layout, Platforms } from '@components';
import styled from 'styled-components';
import { theme, media } from '@styles';
const { colors } = theme;
import Clients from '../images/demo.png';

const PlatformsContainer = styled.section`
  text-align: center;
  width: 100%;
  max-width: 800px;
  margin: auto;
  ${media.tablet`
    padding: 0px;
  `};
  ${media.phoneXL`
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-gap: 10px;
  `};
`;

const Subtitle = styled.h2`
  color: ${colors.lightGrey};
  font-size: 28px;
  line-height: 2;
  font-weight: 500;
  margin-bottom: 20px;
  ${media.tablet`
    font-size: 18px;
  `};
`;

const Subtitle2 = styled.h2`
  color: ${colors.lightGrey};
  font-size: 20px;
  line-height: 2;
  font-weight: 300;
  margin-bottom: 20px;
  ${media.tablet`
    font-size: 18px;
  `};
`;

const Note = styled.p`
  color: ${colors.lightGrey};
  vertical-align: super;
  font-size: small;
  font-weight: 200;
  ${media.tablet`
    font-size: 8px;
  `};
`;

const Links = styled.div`
  text-align: center;
  img {
    padding: 10px;
    min-height: 300px;
    max-width: 800px;
    ${media.tablet`min-height: 300px;`};
  }
`;

const IndexPage = () => (
  <Layout>
    <PlatformsContainer>
      <Subtitle>
        Quality engineering means optimal Quality<sup>*</sup>
      </Subtitle>
      <Note><sup>*</sup>Obligatory marketing tagline</Note>
    </PlatformsContainer>
    <Platforms />
    <Links><img src={Clients} alt="Clients" /></Links>
  </Layout>
);

export default IndexPage;
