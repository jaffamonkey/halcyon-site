import { createGlobalStyle } from 'styled-components';
import theme from './theme';
import mixins from './mixins';
const { colors, fonts, fontSizes } = theme;

const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
    width: 100%;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  body {
    margin: 0;
    width: 100%;
    min-height: 100%;
    overflow-x: hidden;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-family: ${fonts.base};
    font-size: ${fontSizes.base};
    line-height: 1.25;
    color: ${colors.white};
    background: ${colors.fg};
    ${mixins.bgGradient};
  }

  a {
    ${mixins.link};
  }

  img {
    width: 100%;
    max-width: 100%;
    vertical-align: middle;
  }

  .markdown
  {
    display: grid;
    grid-template-columns: 150px auto;
    gap: 40px;
    -moz-box-pack: center;
    justify-content: center;
    padding: 20px;
    width: 100%;
    text-align: left;
    max-width: 980px;
    margin: auto;
  }
  
  svg {
    width: 100%;
    height: 100%;
    fill: currentColor;
    vertical-align: middle;
  }

  button {
    outline: 0;
    border: 0;
    cursor: pointer;
  }

  pre {
    width: 100%;
    max-width 600px;
    padding: 20px;
    margin: auto;
    overflow: auto;
    overflow-y: hidden;
    font-size: 12px;
    line-height: 20px;
    background: #171c28;
    border-radius: 15px;
    border: 2px solid #2f3b54;
  }
  pre code {
    padding: 10px;
    margin-left: 0px;
    color: #d7dce2;
  }

  p {
    margin-top: 0;
  }

  #copy {
    width: 0px;
    max-width: 0px;
    max-height: 0px;
    opacity: 0;
  }

  #input {
    display: none;
  }
`;

export default GlobalStyle;
